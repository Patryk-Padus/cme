// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// SMARTMAGE - www.smartmage.pl - Patryk Padus
// Magento 1.X
// Support individual skins
// Version: 1.0.0
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//MAIN MODULES
const gulp  = require('gulp'),
    package_file = 'package.json',
    fs = require('fs'),
    notify = require('gulp-notify'),
    util = require('gulp-util'),
    hub = require('gulp-hub');
    browserSync = require('browser-sync').create('SERWER');

var pkg = JSON.parse(fs.readFileSync(package_file));

gulp.task('server', function (cb) {
    browserSync.init({
        notify: true,
        open: false,
        injectChanges: true, // FIX: Page reload after change css
        serveStatic: ['.'], //Use local files || FIX: Changes are visible in first refresh, not when upload on server
        proxy: pkg.homepage
    }, cb)
});

gulp.task('gulp:message', function () {
    return gulp.src('package.json').pipe(notify({
        title: 'Gulp is running and listening for changes',
        message: 'Scripts | Styles | Templates now will be watching for changes - maks 10s wait',
        wait: true
    }));
});

gulp.task('server:message', function () {
    return gulp.src('package.json').pipe(notify({
        title: 'BrowserSync',
        message: 'BrowserSync is running and listening for changes',
        wait: true
    }));
});

hub(['./skin/frontend/' + pkg.template + '/**/gulpfile.js']);
gulp.task('default', gulp.series('server','server:message','gulp:message',gulp.parallel(pkg.skins)));