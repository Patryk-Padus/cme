let jQuery: any;

(function($, window, document){
    $( document ).ready(function() {

        let $element = $("#form");
        $element.submit(function(e) {
            e.preventDefault();
            let data = {
                 name: $element.find('#name').val(),
                 email: $element.find('#email').val(),
                 text: $element.find('#text').val()
            };

            $.ajax({
                type: "POST",
                url: "email.php",
                data: data,
                success: function(){
                    let text = '<p class="message-success">' + $('#contact-form').data('success') + '</p>';
                    $element.append(text);
                }
            });

            return false;
        });

        $('.hover-action').on('mouseover',function(){
            $('.hover-show').addClass('is-visible');
        }).on('mouseleave',function(){
            $('.hover-show').removeClass('is-visible');
        });

        $('.bottom-bar').on('click',function(){
            $('#contact-form').toggleClass('is-visible');
        })
    });
})(jQuery, window, document);
