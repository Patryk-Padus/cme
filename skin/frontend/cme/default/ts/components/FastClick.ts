let FastClick: any;

(function($,window, document, undefined){
    'use strict';

    $(function() {
        FastClick.attach(document.body);
    });

})(jQuery, window, document, undefined);