// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// SMARTMAGE - www.smartmage.pl - Patryk Padus
// Magento 1.X
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//MAIN MODULES
const gulp  = require('gulp'),
    normalize = require('normalize-path'),
    path = require('path');

//ADDITIONAl MODULES
var $ = require('gulp-load-plugins')({
        pattern : ["gulp-*", "gulp.*", "main-bower-files", "postcss","cssnano","postcss-cssnext"]
    }),
    browserSync = require('browser-sync').get('SERWER');

//VARIABLES
const SKIN = normalize(__dirname).substr(normalize(__dirname).lastIndexOf('/') + 1);

const MODERNIZR_OPTIONS = [
    "setClasses",
    "addTest",
    "html5printshiv",
    "testProp",
    "fnBind"
];
const MODERNIZR_CUSTOM_TESTS = [];
const MODERNIZR_EXCLUDE_TESTS = [];

//MAIN PATHS
const TEMPLATE_PATH = './', //HTML,PHTML,TWIG etc.
    SKIN_PATH = __dirname;

var js_bower = $.mainBowerFiles('**/*.js',{debugging: true}),
    ts_bower = $.mainBowerFiles('**/*.ts'),
    scss_bower = $.mainBowerFiles('**/*.scss');
sass_dir_import = [];

scss_bower.forEach(function(entry) {
    sass_dir_import.push(entry.substr(0,normalize(entry).lastIndexOf('/')));
});

sass_dir_import.push(SKIN_PATH + '/scss');

//ADDITIONAL PATHS
var paths = {
    main: {
        ts: SKIN_PATH + '/ts/script.ts',
        js: SKIN_PATH + 'script.min.js',
        scss: SKIN_PATH + '/scss/style.scss',
        css: SKIN_PATH + '/css/style.css',
        build_js:  'script.js',
        build_modernizr: 'moderinzr.js'
    },
    dir: {
        js: '/js/'
    }
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CSS TASKS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

gulp.task('css:compile', function () {
    return gulp.src(SKIN_PATH + '/scss/**/*.scss')
        .pipe($.sassBulkImport())
        .pipe($.plumber({
            errorHandler: $.notify.onError({
                title: "<%= error.relativePath %>",
                message: "<%= error.messageOriginal %>"
            })
        }))
        .pipe($.sass({
            outputStyle: 'expanded',
            includePaths: sass_dir_import
        }))
        .pipe(gulp.dest(SKIN_PATH + '/css'))
        .pipe(browserSync.stream())
        .pipe($.plumber.stop());
});

gulp.task('css:optimize', function () {
    return gulp.src(SKIN_PATH + '/css/**/*.css')
        .pipe($.postcss([
            $.postcssCssnext({
                features: {
                    autoprefixer: {
                        add: true,
                        remove: false,
                        flexbox: true
                    }
                }
            }),
            $.cssnano({
                autoprefixer: false,
                safe: true
            })
        ]))
        .pipe(gulp.dest(SKIN_PATH + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('css:ie', gulp.series('css:compile'), function () {
    return gulp.src(SKIN_PATH + '/css/**/*.css')
        .pipe($.bless({
            imports: true,
            suffix: '-part'
        }))
        .pipe(gulp.dest(SKIN_PATH + '/css_ie'));
});

gulp.task('css', gulp.series('css:compile', 'css:optimize'), function () {
    return gulp.src(SKIN_PATH + '/css/**/*.css')
        .pipe(browserSync.stream());
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ JS TASKS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

gulp.task('js:clean', function () {
    return gulp.src([
        SKIN_PATH + paths.dir.js + paths.main.build_js,
        SKIN_PATH + '/js/script.min.js'
    ], {read: false})
        .pipe($.clean());
});

gulp.task("js:compile-ts", function () {
    return gulp.src(ts_bower,{base: './'})
        .pipe($.addSrc.append([SKIN_PATH + '/ts/**/*.ts','!' + SKIN_PATH + '/ts/*.ts']),{base: './',read: false}) //DIRECTORIES - ASC ORDER
        .pipe($.addSrc.append([SKIN_PATH + '/ts/*.ts']),{base: './',read: false}) //MAIN FILES - ASC ORDER
        .pipe($.typescript.createProject("package.json")())
        .pipe($.concat('script.js', {newLine: '\n;'},{read: false}))
        .pipe(gulp.dest(SKIN_PATH + '/js'));
});

gulp.task('js:compile-modernizr', function () {
    return gulp.src([SKIN_PATH + '/js/**/*.js'])
        .pipe($.addSrc.prepend(js_bower),{base: './'})
        .pipe($.modernizr(paths.main.build_modernizr,{
            "cache" : true,
            "crawl" : true,
            "customTests": MODERNIZR_CUSTOM_TESTS,
            "excludeTests": MODERNIZR_EXCLUDE_TESTS,
            "options" : MODERNIZR_OPTIONS,
            "dest": SKIN_PATH + '/js/' + paths.main.build_modernizr
        }))
        .pipe(gulp.dest(SKIN_PATH + '/js'));
});

gulp.task('js:optimize', function () {
    return gulp.src(js_bower,{base: './'})
        .pipe($.addSrc.prepend(SKIN_PATH + '/js/' + paths.main.build_modernizr))
        .pipe($.addSrc.append(SKIN_PATH + '/js/' + paths.main.build_js))
        .pipe($.plumber({
            errorHandler: $.notify.onError({
                title: "<%= error.relativePath %>",
                message: "<%= error.messageOriginal %>"
            })
        }))
        .pipe($.concat('script.min.js', {newLine: '\n;'}))
        .pipe($.uglify())
        .pipe(gulp.dest(SKIN_PATH + '/js'))
        .pipe($.plumber.stop())
        .pipe(browserSync.stream());
});

gulp.task('js', gulp.series('js:clean','js:compile-ts','js:compile-modernizr','js:optimize'), function () {
    return gulp.src('/js/' + paths.main.js)
        .pipe(browserSync.stream());
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CODE TASKS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

gulp.task('code', function (done) {
    browserSync.reload();
    done();
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ IMG TASKS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

gulp.task('img', function () {
    return gulp.src([SKIN_PATH + '/images/**/*.svg'])
        .pipe($.svgmin({
            js2svg: {
                pretty: true
            },
            plugins: [{
                removeTitle: true
            }, {
                removeDoctype: true
            }, {
                removeComments: true
            }, {
                removeUnknownsAndDefaults: true
            }, {
                collapseGroups: true
            }, {
                convertColors: {
                    names2hex: true,
                    rgb2hex: true
                }
            }]
        }))
        .pipe(gulp.dest(SKIN_PATH + '/images/'));
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ WATCH TASKS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

gulp.task('watch:styles', function () {
    gulp.watch([SKIN_PATH + '/scss/**/*.scss'],
        gulp.series('css')
    );
});

gulp.task('watch:scripts', function () {
    gulp.watch([SKIN_PATH + '/ts/**/*.ts'],
        gulp.series('js')
    );
});

gulp.task('watch:code', function () {
    gulp.watch([TEMPLATE_PATH + '/*.html'],
        gulp.series('code')
    );
});

gulp.task('watch', gulp.parallel('watch:scripts', 'watch:styles', 'watch:code'));

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DEFAULT TASK ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

gulp.task(SKIN,
    gulp.parallel(
        gulp.series(
            'img',
            'js',
            'css',
            'watch'
        )
    )
);