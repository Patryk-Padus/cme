var FastClick;
(function ($, window, document, undefined) {
    'use strict';
    $(function () {
        FastClick.attach(document.body);
    });
})(jQuery, window, document, undefined);

;var jQuery;
(function ($, window, document) {
    $(document).ready(function () {
        var $element = $("#form");
        $element.submit(function (e) {
            e.preventDefault();
            var data = {
                name: $element.find('#name').val(),
                email: $element.find('#email').val(),
                text: $element.find('#text').val()
            };
            $.ajax({
                type: "POST",
                url: "email.php",
                data: data,
                success: function () {
                    var text = '<p class="message-success">' + $('#contact-form').data('success') + '</p>';
                    $element.append(text);
                }
            });
            return false;
        });
        $('.hover-action').on('mouseover', function () {
            $('.hover-show').addClass('is-visible');
        }).on('mouseleave', function () {
            $('.hover-show').removeClass('is-visible');
        });
        $('.bottom-bar').on('click', function () {
            $('#contact-form').toggleClass('is-visible');
        });
    });
})(jQuery, window, document);
